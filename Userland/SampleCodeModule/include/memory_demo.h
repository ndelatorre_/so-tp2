#ifndef MEM_MANAGER_TEST 
#define MEM_MANAGER_TEST

#include <stdint.h>
#include <clib.h>

#define ALLOCATED 40
#define INITIAL_MEM 4000
#define OFFSET 1500

#define PAGE 4096

void mem_manager_tester();
void free_tester(uint64_t pointers[]);

#endif