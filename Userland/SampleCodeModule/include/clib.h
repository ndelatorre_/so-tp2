#ifndef CLIB_H_
#define CLIB_H_

#include <stdint.h>
#include <stdarg.h>

#define DAY_REG     0X07
#define MONTH_REG   0X08
#define YEAR_REG    0X09
#define HOUR_REG    0X04
#define MIN_REG     0X02
#define SEC_REG     0X00

#define DATE_SEP    '-'
#define TIME_SEP    ':'

#define FOREGROUND      0
#define BACKGROUND      1

void puts(const char * string);
void perror(const char * string);
void putchar(uint8_t character);
void printf(char * str, ...);
uint8_t getchar(void);
char * gets(char * string, uint64_t size);
void clearScreen(void);
char * getDate(char * date);
char * getTime(char * time);
void sleep(uint64_t millis);
void beep(uint32_t frequency, uint64_t millis);
void exit();
uint64_t getTicks();
void drawPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b);

extern uint64_t syscall(uint64_t rax, uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t r9);


/** MEMORY MANAGER **/
void * alloc(uint32_t size);
void free(void * ptr);
void sys_print_memory();

/** PROCESS **/
void sys_new_process(void *rip, const char *process_name, uint64_t param);
void sys_kill_process(int pid );
void sys_ps();
void sys_change_priority(int pid, int new_priority);
void sys_block_process(int pid);


/** SEM **/
void sys_print_sem();

/** PHYLO **/
void sys_add_philosopher();
int sys_get_phylo_state(int phylo);
void sys_remove_philosopher();
void sys_kill_all_phylos();



#endif /* CLIB_H_ */