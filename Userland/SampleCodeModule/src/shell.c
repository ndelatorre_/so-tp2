// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <clib.h>
#include <shell.h>
#include <utils.h>
#include <memory_demo.h>

#define MAX_LENGTH  50


static int command_count = 19;

static char * command_strings[] = {"help", "date", "time", "sleep", "clear", "beep", "door", "div_zero", "inv_op", "mem" ,"memoryDemo" ,"exit", 
                                   "kill","ps","nice","block","test","sem", "phylo"};

void (* command_functions[]) (void) = {help_cmd, date_cmd, time_cmd, sleep_cmd, clear_cmd, beep_cmd, door_cmd, div_zero_cmd, 
                                       inv_op_cmd, print_mem ,memory_demo, exit_cmd, kill_process, ps_cmd, nice_cmd,block_cmd, 
                                       test_process, sem_cmd, phylo_cmd };


static void newLine(){
    putchar('\n');
}

void initShell() {
    initScreen();
    int command = NO_CMD;
    char input[MAX_LENGTH];

    while (command != EXIT_CMD) {
        puts(PROMPT_STRING);
        gets(input, MAX_LENGTH);
        command = getCommand(input);
        executeCommand(command);
        if (command != CLEAR_CMD){
            newLine();
        }   
    }
	
	exit();
}

void initScreen() {
    clearScreen();
    puts("Bienvenido al programa. El comando help lo ayudara\n");
}

int getCommand(char * input){
    for (int i = 0; i < command_count; i++) {
        if (strcmp(input, command_strings[i]) == 0)
            return i;
    }
    return NO_CMD;
}

void executeCommand(int command) {
    if (command != NO_CMD)
        command_functions[command]();
    else
        puts("\nInvalid command");
}

static void loop(){
    while(1){
        puts("A\n");
    }
}

static void loop2(){
    while(1){
        puts("B\n");
    }
}

void test_process(){
    sys_new_process(&loop,"A",0);
    sys_new_process(&loop2,"B",0);

}


void help_cmd() {
    puts("\nLos comandos disponibles son los siguientes: ");
    puts("\nhelp -- Muestra los comandos validos");
    puts("\ndate -- Muestra la fecha actual");
    puts("\ntime -- Muestra la hora actual");
    puts("\nsleep -- Frena el funcionamiento un numero de segundos a ingresar");
    puts("\nclear -- Limpia la consola");
    puts("\nbeep -- Emite un sonido");
    puts("\ndoor -- Hay alguien en la puerta");
    puts("\ndiv_zero -- Ejecuta una division por cero");
    puts("\ninv_op -- Ejecuta una operacion de codigo invalido ");
    puts("\nmem -- Imprime el estado de la memoria ");
	puts("\nmemoryDemo -- Tester para ver funcionamiento del Memory Manager");
    puts("\nkill [pid] -- Mata al proceso de pid enviado por parametro");

	puts("\nps -- Lista los procesos");
	puts("\nnice [pid] -- Cambia la prioridad de un proceso dado su PID ");
	puts("\nblock [pid] -- Pone el estado del proceso en \"BLOCKED\" dado su PID\n\t\t\t\\->Si ya estaba BLOCKED lo pone en READY");
    puts("\ntest -- crea 2 procesos 'A' y 'B', que imprimen 'A' y 'B' respectivamente\n\t\t\t\\->mientras corre podemos ejecutar comandos como 'nice', 'block', 'kill' 'ps' para ver funcionalidades");

    puts("\nsem -- Imprime los semaforos activos ");
    puts("\nphylo -- Problema de los filosofos comensales, arranca con 1 y como maximo pueden ser 5");
    puts("\nexit -- Termina la ejecucion");
}

void date_cmd() {
    char date[11];
    printf("\nHoy es %s", getDate(date));
}

void time_cmd() {
    char time[9];
    puts("\nSon las  ");
	puts(getTime(time));
}

void sleep_cmd() {
    char car;
    do{
        puts("\nIngrese el numero de segundos que desea esperar [0-9]: ");
        car = getchar();
        putchar(car);
    } while (!isNumber(car));
    int millis = (car - '0') * 1000;
    sleep(millis);
}

void clear_cmd() {
    clearScreen();
}

void beep_cmd() {
    beep(BEEP_FREQ, 300);
}

void door_cmd() {
    beep(DOOR_FREQ, 300);
    sleep(300);
    beep(DOOR_FREQ, 150);
    sleep(150);
    beep(DOOR_FREQ, 150);
    sleep(100);
    beep(DOOR_FREQ, 150);
    sleep(250);
    beep(DOOR_FREQ, 150);
    sleep(800);
    beep(DOOR_FREQ, 150);
    sleep(150);
    beep(DOOR_FREQ, 150);
}

void div_zero_cmd() {
    int a = 10, b = 0;
    a = a / b;
    printf("%d", a);
}

void inv_op_cmd() {
    uint64_t invalid = 0xFFFFFFFFFFFF;
	uint64_t * ptr = &invalid;
	((void(*)())ptr)();
}

void exit_cmd() {
    puts("\nHasta Luego");
}



void memory_demo(){
	mem_manager_tester();
}

void print_mem(){
	sys_print_memory();
}


static int get_pid(){
    puts("\nPID: ");
	char s[100];
	int i=0;
	char c;
	while( (c = getchar()) != '\n'){
		if (c!='\b' && (isAlpha(c) || isNumber(c) || c ==' ' )){ 
			putchar(c);
			if(i<MAX_LENGTH)
				s[i]=c;
			i++; 
		}
		if(c=='\b' && i!=0){ 
			putchar(c);
			i--;
		}
	}
	s[i]=0;
	return atoi(s,i);
}

static int get_priority(){
    puts("\nNEW PRIORITY: ");
	char s[100];
	int i=0;
	char c;
	while( (c = getchar()) != '\n'){
		if (c!='\b' && (isAlpha(c) || isNumber(c) || c ==' ' )){ 
			putchar(c);
			if(i<MAX_LENGTH)
				s[i]=c;
			i++; 
		}
		if(c=='\b' && i!=0){ 
			putchar(c);
			i--;
		}
	}
	s[i]=0;
	return atoi(s,i);
}


void kill_process(){
	int pid = get_pid();
	if(pid != 0){
		sys_kill_process(pid);
	}
	else{
		puts("\nNo se puede eliminar el proceso shell \n");
	}
}


void ps_cmd(){
    sys_ps();
}

void nice_cmd(){
    int pid = get_pid();
	if(pid != 0){
        //puts("\n ~ TODO: ~ TENEMOS QUE LLAMAR A LA SYSCALL QUE CAMBIE LA PRIORIDAD DEL PROCESO\n");
        int new_priority = get_priority();
        sys_change_priority(pid, new_priority);
        // printf("\npid : %d \n",pid);
        // printf("\npriority : %d \n",new_priority);
	}
	else{
		puts("\nNo se puede cambiar la prioridad del proceso shell\n");
	}
}

void block_cmd(){
    int pid = get_pid();
	if(pid != 0){
        //puts("\n ~ TODO: ~ TENEMOS QUE LLAMAR A LA SYSCALL QUE PONGA EL PROCESO EN BLOCKED\n");
        sys_block_process(pid);
        //printf("\npid : %d \n",pid);
	}
	else{
		puts("\nNo se puede bloquear el proceso shell \n");
	}
}


void sem_cmd(){
    sys_print_sem();
}

void phylo_cmd(){
    char *states[] = {"THINKING", "HUNGRY", "EATING"};
    sys_add_philosopher();
    int curr_phylos = 1;
    printf("\nProblema de los filosofos. \n");

    printf("\nSelecciona:\n");
    printf("\t'a' para agregar un filosofo.\n");
    printf("\t't' para ver el estado de la mesa.\n");
    printf("\t's' para imprimir los semaforos activos. \n");
    printf("\t'r' para eliminar un filosofo.\n");
    printf("\t'p' para correr el programa 'ps', y ver los procesos activos.\n");
    printf("\t'q' para salir del programa.\n");

    printf("\n\nFilosofos actuales: %d \n", curr_phylos);


    char input[50];
    while (1) {
        printf("\nIngrese una instruccion: ");
        gets(input, 50);
        printf("\n\n");
        if ( input[0] == 'a' ) {
            if(curr_phylos < 5){
                sys_add_philosopher();
                curr_phylos++;
                printf("Filosofos actuales: %d \n", curr_phylos);
            }
            else{
                printf("No puede haber mas de 5 filosofos\n");
            }
        }
        else if (input[0] == 't'){
            int state;
            for (int i = 0; i < curr_phylos; i++){
                state = sys_get_phylo_state(i);
                printf("\nPhylo - %d =",i);
                printf(" %s",states[state]);
            }
            printf("\n");
           
        }
        else if (input[0] == 's'){
            sem_cmd();
        }
        else if (input[0] == 'r'){
            if(curr_phylos != 1){
                sys_remove_philosopher();
                curr_phylos--;
                printf("Filosofos actuales: %d \n", curr_phylos);
            }
            else {
                printf("Debe haber al menos 1 filosofo, si quiere salir del programa ejecute 'q'\n");
            }
        }
        else if (input[0] == 'p'){
            ps_cmd();
        }
        else if (input[0]=='q'){
            sys_kill_all_phylos();
            return;
        }
        else {
            printf("\nError, comando invalido, pruebe con alguna de las siguientes instrucciones \n");
            printf("\t'a' para agregar un filosofo.\n");
            printf("\t't' para ver el estado de la mesa.\n");
            printf("\t's' para imprimir los semaforos activos. \n");
            printf("\t'r' para eliminar un filosofo.\n");
            printf("\t'p' para correr el programa 'ps', y ver los procesos activos.\n");
            printf("\t'q' para salir del programa.\n");
        }
    }
}

