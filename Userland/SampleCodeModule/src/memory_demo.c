#include "../include/memory_demo.h"


void mem_manager_tester(){
	uint64_t pointers[ALLOCATED];
	uint64_t memory[ALLOCATED];
	int i;
	int mem = INITIAL_MEM;

	for (i = 0; i < ALLOCATED; i++){
		if((pointers[i] = (uint64_t)alloc(mem + i * OFFSET)) == (uint64_t)-1){ //casteos por los warnings
			puts("\nERROR: Error en el malloc, no hay suficiente memoria\n");
		} 
		uint64_t size = mem + i * OFFSET;
		uint64_t blocks = 0;
		while (size > PAGE){
			size -= PAGE;
			blocks++;
		}
		if (size != 0) blocks++;

		memory[i] = blocks * PAGE;


	}

	puts("\nTodos los punteros fueron creados\n");

	sys_print_memory();

	// me fijo que no haya 2 en la misma posicion
    int j;
	for (i = 0; i < ALLOCATED - 1; i++){
		for (j = i + 1; j < ALLOCATED; j++){
			// printf("i: %d -- pointer[i]: %d -- pointer[j]: %d",i,pointers[i],pointers[j]);
			if (pointers[j] >= pointers[j] && pointers[j] < pointers[i] + memory[i]){
				puts("\nERROR: Dos punteros en la misma posicion!");
			}
		}
	}
	puts("\nNo habia dos punteros en la misma posicion\n");


	puts("\nLibero todo antes de irme\n");


	// libero todo antes de terminar
	free_tester(pointers);

	puts("\nListo\n");

    puts("\nImprimo la memoria para ver que se libero todo\n");

    sys_print_memory();

	return;
}



void free_tester (uint64_t pointers[]){
	uint64_t i;
	for (i = 0; i < ALLOCATED ; i++){
		free((void *)pointers[i]);
	}
	return;
}

