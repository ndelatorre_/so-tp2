#ifndef PHYLO___H___
#define PHYLO___H___

#define N 5 
#define LEFT(i)  ( i + N-1 ) % N
#define RIGHT(i) ( i + 1 ) % N 


typedef enum {THINKING = 0, HUNGRY, EATING } phylo_state;

int init_phylosopher();
void philosopher(int phylo);
void set_eating(int phylo);
void set_thinking(int phylo);
int add_phylosopher();
int remove_phylosopher();

void take_forks(int phylo);
void put_forks(int phylo);
void try_to_eat(int phylo);
int get_phylosopher_state(int phylo);

void kill_all_phylos();

#endif