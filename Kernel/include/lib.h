#ifndef LIB_H
#define LIB_H

#include <stdint.h>



void * memset(void * destination, int32_t character, uint64_t length);
void * memcpy(void * destination, const void * source, uint64_t length);



//todas estas están en libasm.asm
char * cpu_vendor(char *result);

char read_port(char port);
void write_port(char port, char reg);

void * set_stack(void *addr,void *rip, uint64_t rdi);
void add_sem(void *addr);
void delete_sem(void *addr);

#endif