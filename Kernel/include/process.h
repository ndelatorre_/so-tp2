#ifndef PROCESS_H_
#define PROCESS_H_


#define MAX_PID 50
typedef enum {READY = 0, RUNNING, BLOCKED, UNDEFINED} states;


typedef struct process {
    states status;
    unsigned char ppriority;
    void *base_pointer;
    void *stack_pointer;
    struct process * next;
    char *name;
} process_t;

//struct para scheduler
typedef struct node {
    int pid;
    struct node *next;
} node_t;


void *schedule(void *prev_rsp);

int change_process_priority(int pid, int priority);

int change_process_state(int pid, char state);

int create_process(int priority, void *rip, char* name, int rdi);

int get_pid(); 

int kill(int pid);

void print_process();

int get_Process_Pid(process_t* process);

process_t* get_Pnode(int pid);

int addProcess_scheduler(int pid);

int removeProcess_scheduler( int pid );

int get_free_pid();

int is_blocked(int pid);


#endif