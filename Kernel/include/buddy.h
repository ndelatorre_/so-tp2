#ifndef _BUDDY_MANAGER_H_
#define _BUDDY_MANAGER_H_

#include <stddef.h>
#include <stdint.h>


#define MEM_SIZE 0x200000	// numero cualquiera
#define PAGE_SIZE 4096	// 4K
#define MEM_ARRAY_SIZE MEM_SIZE/PAGE_SIZE
#define MAX_BUDDIES 21

#define TRUE 1
#define FALSE 0


typedef struct {
    char isAllocated;               
    uint64_t size;                  
    uint64_t buddies[MAX_BUDDIES]; 
    uint64_t currentBuddy;        
} memBlock;

//función para inicializar la memoria dado un puntero inicial y el tamaño total de la memoria
void initializeMemoryManager();

//retorna el puntero del nodo adecuado
void * alloc(uint64_t size);

//función que llama a liberar el nodo que tenga el puntero apuntando a ptr
void free(void * ptr);

//devuelve un puntero al inicio de la memoria
void * memoryStart();

void printMemory();

void partitionBlock(uint64_t startIdx, uint64_t buddyIdx);
uint64_t getBuddyOffset(uint64_t blockSize);
uint64_t mergeMemory(uint64_t index);


#endif