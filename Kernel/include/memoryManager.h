#ifndef _MEM_MANAGER_H_
#define _MEM_MANAGER_H_

#include <stddef.h>
#include "videoDriver.h"


#define MEM_SIZE 0x200000	// numero cualquiera
#define PAGE_SIZE 4096	// 4K
#define MEM_ARRAY_SIZE MEM_SIZE/PAGE_SIZE

#define TRUE 1
#define FALSE 0


typedef struct {
	char allocated;			// Para marcar si el bloque esta allocado o no, 1 si, 0 no
	uint64_t size;
} memBlock;


//función para inicializar la memoria dado un puntero inicial y el tamaño total de la memoria
void initializeMemoryManager();

// función que llama a find_first_fit con el nodo raíz y retorna el puntero del nodo adecuado
void * alloc(uint64_t size);

//función que llama a liberar el nodo que tenga el puntero apuntando a ptr
void free(void * ptr);

//devuelve un puntero al inicio de la memoria
void * memoryStart();

void printMemory();


#endif
