#ifndef SYSCALLS_H_
#define SYSCALLS_H_

#include <stdint.h>
#include <lib.h>
#include <sem.h>

void read_handler(uint64_t fd, char * buff, uint64_t count);
void write_handler(uint64_t fd, const char * buff, uint64_t count);
uint64_t time_handler();
void clear_handler();
uint64_t rtc_handler(uint8_t register);
void sleep_handler(uint64_t millis);
void beep_handler(uint16_t frequency, uint64_t time);
void pixel_handler(uint64_t x, uint64_t y, uint64_t rgb);

uint64_t handleSyscall(uint64_t sirq, uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t r9);

void * malloc_handler(uint64_t bytes);
void free_handler(void * ptr);
void print_memory();

/* PROCESOS */
void new_process_handler(void * rdi,char * process_name, uint64_t parameters);
void kill_handler(int pid);
void ps_handler();
void change_priority_handler(int pid, int new_priority);
void change_state_handler(int pid);

/** SEM **/
void print_sem_handler();

/** PHYLO **/
void modify_table_handler(int flag);
uint64_t get_phylo_state_handler(int phylo);
void kill_all_phylos_handler();


#endif

