#ifndef ___SEM___H___
#define ___SEM___H___

#include <process.h>
#include <stdint.h>

typedef struct blocked{
    int pid;
    struct blocked * next;
    
} p_blocked;


typedef struct sem_data {
    uint64_t id;
    uint64_t count;
    struct sem_data * next;
    p_blocked * process_blocked;

} semaphore;


int sem_init(int id, int count);
semaphore * sem_open(int id);
int sem_wait(int id);
int sem_post(int id); 
void print_sem();
int sem_close(int id);

#endif