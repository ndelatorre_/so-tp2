#include <lib.h>
#include <process.h>
#include <strings.h>
#include <memoryManager.h>
#include <sem.h>
#include <stdint.h>
#include <interrupts.h>


semaphore * first = NULL;


int sem_init(int id, int initial_count ) {

    semaphore  * sem;
    if ( first == NULL ) {
       first = alloc(sizeof(semaphore));
       sem = first;
    }
    else {
        sem = first;
        while(sem->next != NULL){
            sem = sem->next;
        }

        sem->next = alloc(sizeof(semaphore));
        sem = sem->next;
    }

    sem->id = (uint64_t) id;
    sem->count = (uint64_t) initial_count;
    sem->process_blocked = NULL;
    sem->next = NULL;

    return 0;
}

semaphore *sem_open(int id) {

    semaphore *current = first;
    while( current != NULL && current->id != (uint64_t) id ) { 
        current = current->next;
    }

    return current;
}

int sem_wait(int id) {

    semaphore *sem = sem_open(id);
    if (sem == NULL) {
        return 1;
    }

    if ( sem->count == 0 ){

        int pid = get_pid();
        if( pid != 0 && pid != 1){ //me fijo que no este corriendo shell  ni init por las dudas, sino puede colgar todo
            p_blocked * pblocked;
            if ( sem->process_blocked == NULL ) {
                sem->process_blocked = alloc(sizeof(p_blocked));
                pblocked = sem->process_blocked;
            }
            else {
                pblocked = sem->process_blocked;
                while(pblocked->next != NULL){
                    pblocked = pblocked->next;
                }
                
                pblocked->next = alloc(sizeof(p_blocked));
                pblocked = pblocked->next;
            }

            pblocked->pid = pid;
            pblocked->next = NULL;


            change_process_state(pid, BLOCKED);
            _hlt();

            p_blocked * prev;
            p_blocked * c = sem->process_blocked;
            while (c->next != NULL && c->pid != pid ){   
                prev = c;
                c = c->next;
            }
            if (c->pid == pid){
                prev->next = c->next;
            }else{
                return 2;
            }        
        } else {
            delete_sem( &(sem->count) );
        }
        if (sem->count >= 1) {
            delete_sem( &(sem->count) );
        }
    }

    return 0;
}

int sem_post( int id ) {

    semaphore *sem = sem_open(id);
    if (sem == NULL) {
        return 1;
    }

    add_sem( &(sem->count) );

    if (sem->count <= 1) {

        if( sem->process_blocked != NULL ) {
            p_blocked * aux = sem->process_blocked->next;
            change_process_state(sem->process_blocked->pid, READY);
            sem->process_blocked = aux;
        }
    }

    return 0;
}



int sem_close(int id) {

    semaphore * prev;
    semaphore * current = first;
    while (current != NULL && current->id != (uint64_t) id){
        prev = current;
        current = current->next;
    }
    if (current == NULL){
        return 1;
    }
    else{
        prev->next = current->next;
        free(current);
        return 0;
    }
}


void print_sem() {
    int flag = 0; 
    if(first == NULL){
        print("\nNo hay ningun semaforo\n");
        return ;
    }
    semaphore * aux = first;
    while( aux != NULL){
        print("\nSemaforo: %d", aux->id);    
        p_blocked * p_aux = aux->process_blocked;    
        while (p_aux != NULL){
            print("\t\tBloquea los procesos con PID: ");
            print("%d -- ", p_aux->pid);
            p_aux = p_aux->next;
        }
        print("\n");
        aux = aux->next;
    }
    return 0;
}

