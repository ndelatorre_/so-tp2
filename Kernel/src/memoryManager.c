#if (MEM_MANAGER == 0)
 
#include "memoryManager.h"
#include <console.h>



//	********* FIRST FIT *********
static void * const memStartAddress = (void *)0x900000;		
static memBlock memory[MEM_ARRAY_SIZE];

void initializeMemoryManager(){
	int i;
	memory[0].allocated = FALSE;
	memory[0].size = MEM_ARRAY_SIZE;
	for(i = 1 ; i< MEM_ARRAY_SIZE ; i++){
		memory[i].allocated = FALSE;
		memory[i].size = 0;	
        //print("allocated: %d -- size: %d \n",memory[i].allocated,memory[i].size );
	}
}


//
// Recibe la cant de paginas que necesita. 
// Retorna el indice de donde esta o -1 si FALSE habia lugar.
//
uint64_t firstFit (uint64_t pages) {
	uint64_t index;

	for (index = 0; index < MEM_ARRAY_SIZE; index++) {
		
		if (memory[index].size >= pages) {
			if (memory[index].allocated == FALSE) {
				return index;
			}
		}
	}
	
	return -1; 	// FALSE habia lugar
}


//  Recibe el tamaño que necesita. 
void * alloc (uint64_t size){ 
	if (size <= 0 || size > MEM_SIZE) {
		return -1;
	}

	//Calculo las paginas que necesito para mandarselas a firstfit 
	uint64_t pages = size / PAGE_SIZE;

	if (size % PAGE_SIZE != 0) {
		pages++;
	}


	uint64_t start = firstFit(pages);

	if (start == -1) {
		// preguntar si hay que mostrar algun mensaje de error o que 
		return -1;	// Si me devolvio que FALSE habia lugar, retorFALSE -1
	}

	uint64_t blockSize = memory[start].size;

	//Pongo el bloque en "allocado" para que FALSE lo pise otro proceso 
	uint64_t i;
	for (i = start; i < (start + pages); i++) {
		memory[i].allocated = TRUE;
		memory[i].size = 0;
	}

	memory[start].size = pages;	


	if (blockSize > pages) {
		memory[start + pages].size = blockSize - pages;	
		memory[start + pages].allocated = FALSE;			
	}

	void * returnPtr = memStartAddress + start * PAGE_SIZE;
	return returnPtr;
}

void free (void * p) {


	//  calculo donde esta la pagina 
    uint64_t pointer = (uint64_t) p;
	uint64_t startIdx = (pointer - (uint64_t)memStartAddress)/PAGE_SIZE;	

	uint64_t blockSize = memory[startIdx].size;


	// pongo los bloques en falso para que lo pueda usar otro proceso
	uint64_t i;
	for (i = startIdx; i < (startIdx + blockSize); i++) {
		memory[i].allocated = FALSE;
	}


	uint64_t nextBlock = startIdx + blockSize;

	if (nextBlock < MEM_ARRAY_SIZE) { // chequeo que hay mas bloques despues del que voy a liberar
		if (memory[nextBlock].allocated == FALSE){ // si el siguiente tambien esta libre hago un solo bloque mas grande
			memory[startIdx].size = blockSize + memory[nextBlock].size; 
			memory[nextBlock].size = 0;
		}
	}

	if (startIdx > 0) {

		if (memory[startIdx - 1].allocated == FALSE) {
			uint64_t i = startIdx;
			do {
				i--;
			} while (memory[i].size == 0);

			memory[i].size = memory[i].size + memory[startIdx].size;
			memory[startIdx].size = 0;	
		}

	}

}




void printMemory(){
	
    int i;
	print("\nMEMORY FIRST FIT: \n");
	for( i = 0 ; i < MEM_ARRAY_SIZE ; ++i ){
		if(memory[i].size != 0){
			print("%d",i);
			print("->");
			print("[");
			print("%d",memory[i].size);
			print(",");
			if(memory[i].allocated == 0){
				print("FREE");
			}
			else {
				print("ALLOCATED");

			}
			print("]\n");
		}
	}
	

	print("\nEND\n");

} 

 

#endif



/* ******************************************************************
   ******************************************************************
   ****************************************************************** 
   ******************************************************************/



#if (MEM_MANAGER == 1)
//	********* BUDDY SYSTEM *********
#include "include/buddy.h"
#include <console.h>



static void * const memStartAddress = (void *)0x900000;     
static memBlock memory[MEM_ARRAY_SIZE] = {{0, MEM_SIZE, {0}, -1},{0, 0, {0}, -1}};;  //esto deberia ir en el initialize


void initializeMemoryManager(){
    
}


void * alloc(uint64_t size){
    //Verify if input is legal
    if (size <= 0 || size > MEM_SIZE) {
        return -1;
    }

    uint64_t allocIdx = 0;     //index

    //Recorro memory hasta encontrar lugar libre
    while (allocIdx < MEM_ARRAY_SIZE && (memory[allocIdx].size < size || memory[allocIdx].isAllocated)){
        allocIdx++;
    } 
    

    memBlock * allocBlock = &memory[allocIdx];  
    uint64_t buddyOffset;                      

    //veo cuantas paginas necesito 
    uint64_t blocks = size / PAGE_SIZE;
    if (size % PAGE_SIZE != 0) {        
        blocks++;
    }

    if (allocBlock->size > PAGE_SIZE){

		// divido el bloque a la mitad hasta que no entre mas 
        while (allocBlock->size/2 >= PAGE_SIZE && (allocBlock->size)/2 >= size){
            buddyOffset = getBuddyOffset(allocBlock->size);
            partitionBlock(allocIdx, allocIdx + buddyOffset);
        }   
    
        for (int i = allocIdx; blocks > 0 && i < MEM_ARRAY_SIZE; blocks--, i++){
            memory[i].isAllocated = TRUE;
        }
    } else {
        allocBlock->isAllocated = TRUE;
    }

    void * returnPtr = (memStartAddress + allocIdx * PAGE_SIZE);
    return returnPtr;
}


void free (void * ptr) {

    uint64_t freeIdx = ((uint64_t)ptr - (uint64_t)memStartAddress)/PAGE_SIZE;    
    
    memBlock * freeBlock = &memory[freeIdx];

    if (freeBlock->size == 0 && !freeBlock->isAllocated){
        return;      
    }

    freeBlock->isAllocated = FALSE;

    for (int i = freeIdx + 1; i < MEM_ARRAY_SIZE && memory[i].size == 0; i++){
        memory[i].isAllocated = FALSE;
    }
         
    mergeMemory(freeIdx);
    return;
}



void partitionBlock(uint64_t startIdx, uint64_t buddyIdx){
    memBlock * startBlock = &memory[startIdx];
    memBlock * buddyBlock = &memory[buddyIdx];

    uint64_t currSize = (startBlock->size);
    startBlock->size = currSize/2;
    buddyBlock->size = currSize/2;

    startBlock->buddies[++(startBlock->currentBuddy)] = buddyIdx;
    buddyBlock->buddies[++(buddyBlock->currentBuddy)] = startIdx;

    return;
}

uint64_t getBuddyOffset(uint64_t blockSize){
    return (blockSize/2)/PAGE_SIZE; 
}

uint64_t mergeMemory(uint64_t index){
    uint64_t mergeIdx, buddyIdx;

    if (memory[index].currentBuddy == -1) return 0;

    buddyIdx = memory[index].buddies[memory[index].currentBuddy];

    if (memory[buddyIdx].isAllocated || memory[buddyIdx].size != memory[index].size) return 0;

    if (buddyIdx < index){
        mergeIdx = buddyIdx;
        buddyIdx = index;
    } else {
        mergeIdx = index;
    }

    memBlock * mergeBlock = &memory[mergeIdx];
    memBlock * buddyBlock = &memory[buddyIdx];

    mergeBlock->size += buddyBlock->size;  
    (mergeBlock->currentBuddy)--;
    buddyBlock->size = 0;
    (buddyBlock->currentBuddy)--;

    if (mergeBlock->currentBuddy != -1){
        if (mergeMemory(mergeIdx) == -1) return -1;
    }

    return 0;
}

void printMemory() {
    int index;
    print("\nMEMORY BUDDY: \n");
    for (index = 0; index < MEM_ARRAY_SIZE; ++index) {
        if (memory[index].size != 0) {
            print("%d",index);
            print("->");
            print("[");
            print("%d",memory[index].size);
            print(",");
            if(memory[index].isAllocated == 0){
                print("FREE");
            }
            else {
                print("ALLOCATED");
            }
            print("]\n");
        }
    }
    print("END\n");
}

 
 #endif