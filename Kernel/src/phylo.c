#include <lib.h>
#include <strings.h>
#include <memoryManager.h>
#include <phylo.h>
#include <sem.h>
#include <process.h>
#include <syscalls.h>
#include <stdint.h>
#include <interrupts.h>


#define ID_PHYLO 500 

int phylo_states[N]; 
int pids[N];
int mutex;  
int sem[N]; 
int phylos = 0;

int init_phylosopher() {
    mutex = ID_PHYLO + 1;
    int res = sem_init(mutex, 1);
    
    if (res != 0) {
        return 1; 
    }
    
    void * res_open = sem_open(mutex);
    if(res_open == NULL) {
        return 2;
    }

    return 0;
}

void philosopher(int phylo) { 
    sem[phylo] = ID_PHYLO + phylo * ID_PHYLO;

    sem_init(sem[phylo], 0);
    sem_open(sem[phylo]);

    while (1) {
        // print("\nAAAAA\n");
        set_thinking(phylo);
        // print("\nBBBBBB\n");
    
        take_forks(phylo);      
        // print("\nCCCCC\n");

        set_eating(phylo);      
        put_forks(phylo);      
    } 
}

void set_thinking(int phylo){
    phylo_states[phylo] = THINKING;
}

void set_eating(int phylo){
    phylo_states[phylo] = EATING;
}

void take_forks(int phylo){

    sem_wait(mutex);

    phylo_states[phylo] = HUNGRY;      
    try_to_eat(phylo);                

    sem_post(mutex);
    sem_wait(sem[phylo]);
}

void put_forks(int phylo) {     
   
    sem_wait(mutex);
    phylo_states[phylo] = THINKING;    
    //me fijo si puedo ponerlo en eating
    try_to_eat(LEFT(phylo));             
    try_to_eat(RIGHT(phylo));            

    sem_post(mutex);
 }


void try_to_eat(int phylo){

    if (phylo_states[phylo] == HUNGRY && phylo_states[LEFT(phylo)] != EATING && phylo_states[RIGHT(phylo)] != EATING) { 
        phylo_states[phylo] = EATING;
        sem_post(sem[phylo]);
    }
}

int get_phylosopher_state(int phylo) {
    return phylo_states[phylo];
}

int add_phylosopher(){
    if ( phylos == 0 ){
        init_phylosopher();
    }
    if (phylos >= N ) {
        return 1; //no puede haber mas de 5 phylos
    } else {
        pids[phylos] = create_process(5, &philosopher, "philosopher", phylos);
        phylos++;
        return 0;
    }
}

int remove_phylosopher(){
    if ( phylos == 0 ) {
        return 1;
    }
    if(phylos > 0){
        kill(pids[phylos]);
        sem_close(sem[phylos]);
        phylos--;
    }
    return 0;
}



void kill_all_phylos(){
    for(int i = 0 ; i < phylos ; i++){
        kill(pids[i]);
        sem_close(sem[i]);
    }
    sem_close(mutex);
    phylos = 0;
}
