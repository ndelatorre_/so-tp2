// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <syscalls.h>
#include <keyboard.h>
#include <time.h>
#include <console.h>
#include <naiveConsole.h>
#include <sound.h>

#include <memoryManager.h>
#include <process.h>
#include <sem.h>
#include <phylo.h>


#define ADD 1
#define REMOVE 0

void read_handler(uint64_t fd, char * buff, uint64_t count) {
    // File descriptor doesn't matter
    for (int i = 0; i < count; i ++)
        buff[i] = read_character();
}

void write_handler(uint64_t fd, const char * buff, uint64_t count) {
    switch (fd) {
        case STDIN:
        case STDOUT:
            print_N(buff, count);
            //ncPrint(buff);
            break;
        case STDERR:
            printError_N(buff, count);
            //ncPrintError(buff);
            break;
        default:
            break;
    }
}

uint64_t time_handler() {
    return ticks_elapsed();
}

void clear_handler() {
    clear_console();
	// ncClear();
}

uint64_t rtc_handler(uint8_t reg){
    write_port(0x70, reg);
    uint64_t aux = read_port(0x71);
    return (aux >> 4) * 10 + (aux & 0x0F);
}

void sleep_handler(uint64_t millis){
    sleep(millis);
}

void beep_handler(uint16_t frequency, uint64_t millis) {
    play_sound(frequency);
    sleep_handler(millis);
    no_sound();
}

void pixel_handler(uint64_t x, uint64_t y, uint64_t rgb) {
    Vector2 auxPos = {x, y};
    Color auxColor = {(rgb & 0xFF0000) >> 16, (rgb & 0x00FF00) >> 8, rgb & 0x0000FF};
    draw_pixel(auxPos, auxColor);
}

/** MEMORY MANAGER **/
void * malloc_handler(uint64_t bytes) {
    return alloc(bytes);
}

void free_handler(void * ptr) {
    free(ptr);
}

void print_memory() {
    printMemory();
}


/** PROCESS **/
/** TODO: AGREGAR LAS FUNCIONES DE PROCESOS ACA Y EN EL .h **/
void new_process_handler(void * rip,char * process_name, uint64_t parameters){
    create_process(5,rip,process_name,parameters);
}




void kill_handler(int pid){
    //print("HASTA ACA LLEGA BIEN\n");
    kill(pid);
}

void ps_handler(){
    //print("HASTA ACA LLEGA BIEN\n");
    print_process();
}

void change_priority_handler(int pid, int new_priority){
    change_process_priority(pid, new_priority);
}


void change_state_handler(int pid){
    if(is_blocked(pid)){
        change_process_state(pid,READY);
    }
    else{
        change_process_state(pid,BLOCKED);
    }
    
}


void print_sem_handler(){
    print_sem();
}


void modify_table_handler(int flag){
    if(flag == ADD){
        add_phylosopher();
    }
    else if( flag == REMOVE){
        remove_phylosopher();
    }
}

uint64_t get_phylo_state_handler(int phylo){
    return get_phylosopher_state(phylo);
}

void kill_all_phylos_handler(){
    kill_all_phylos();
}


