#include <stdint.h>
#include <lib.h>
#include <strings.h>
#include <process.h>
#include <memoryManager.h>
#include <interrupts.h>
#include <time.h>


#define STACK_SIZE 2000
#define MAX_PID 50
//Minima cantidad de timer ticks
#define QUANTUM 3


// Este .c va a crear un proceso nuevo y administrar los existentes. 
// Para que esto suceda se va a llevar un trackeo de todos los procesos en un array de structs. 
// Teniendo en cada struct toda la informacion de cada proceso. 
// Esto va a permitir cambiar el proceso que corre facilmente.

int get_free_pid();


process_t *process_list[MAX_PID]; 


// contador actual de prioridad.
int prior_counter = -1;

// Flag que dice si el scheduler esta andando o no. Sirve para el caso de que no tenga ningun
// proceso en la lista y no rompa.
int scheduler = 0;
int virgin = 1;

//Creo el HEAD y el CURRENT de la lista del scheduler.
node_t * first_process = NULL;
node_t * current_process = NULL;


int create_process(int priority, void *rip, char *name, int rdi) { 

    int pid = get_free_pid();
    if(pid == -1) {
        return -1;
    }

    process_t * aux = (process_t *) alloc( sizeof(process_t) );
    if( aux == NULL ){
        return -1;
    }

    aux->ppriority = priority;
    aux->status = READY;
    aux->name = name;


    void *process_stack = alloc( STACK_SIZE );
    if ( process_stack == NULL ) {
        return -1;
    }

    if ( pid!= 0) {

        process_list[pid]= aux;
        process_list[pid]->base_pointer = process_stack;
        process_list[pid]->stack_pointer = process_list[pid]->base_pointer + STACK_SIZE ;

        // Seteo el STACK, le pasamos el SP y el IP.
        void *updated_sp = set_stack(process_list[pid]->stack_pointer,rip,rdi);
        process_list[pid]->stack_pointer = updated_sp;

        // Agrego el proceso a la lista del scheduler.
        node_t *process_node = (node_t *) alloc(sizeof(node_t));
        if(process_node == NULL) {
            return -1;
        }

        process_node->pid = pid;
        process_node->next = NULL;
        
        //Seteo current al primer proceso de scheduler
        node_t *current = first_process;
        
        if (current == NULL) { 
            // primer proceso en lista.
            first_process =  process_node;
            current_process = first_process;

            scheduler = 1;    //prendo el flag por que ya hay procesos en el scheduler
        } else {
            //no es el primer proceso  ==> lo agregamos al final de la lista
            while(current->next != NULL ) {
                current=current->next;
            }
            current->next = process_node;
        }
        
        
    } else {
        // si pid es 0, es init()
        process_list[pid]= aux;
        
        process_list[pid]->base_pointer = process_stack;
        process_list[pid]->stack_pointer = process_list[pid]->base_pointer + STACK_SIZE ;
        void *updated_sp = set_stack(process_list[pid]->stack_pointer,rip,rdi);
        process_list[pid]->stack_pointer = updated_sp;

    }

    return pid; //devuelvo el pid del proceso
}




void *schedule(void *prev_rsp) {


    if ( !scheduler ) {
        return prev_rsp;
    }

    if ( prior_counter >= 0 ) {
        prior_counter--;
        return prev_rsp;
    }



    if ( first_process == NULL) {
        return process_list[0]->stack_pointer;
    }



    if ( current_process == NULL ) {
        int pid = get_pid();
        if ( pid == -1 ) {
            // no se encontró un proceso que esté corriendo.

        } else {
            process_list[pid]->status= BLOCKED;
            process_list[pid]->stack_pointer = prev_rsp;
        }
        // devuelvo el stack pointer de first
        current_process = first_process;
        process_list[first_process->pid]->status = RUNNING;
        return process_list[first_process->pid]->stack_pointer;

    }


    int current_pid = current_process->pid;
    
    if ( !virgin ) {
        process_list[current_pid]->stack_pointer = prev_rsp;
    } else {
        virgin = 0;
    }
    process_list[current_pid]->status = READY; // Actualizo el SP del proceso viejo para la proxima vez que se llame
    

    // Paso al proximo proceso 
    if (current_process->next == NULL) {
        current_process = first_process;
    }
    else {
        current_process = current_process->next;
    }
    
    int new_pid = current_process->pid;
    process_list[new_pid]->status = RUNNING; //Actualizo el estado del nuevo
    
    prior_counter = QUANTUM + process_list[new_pid]->ppriority;

    return process_list[new_pid]->stack_pointer;
}

int get_free_pid() {
    int i;
    for (i = 0; i < MAX_PID; i++){
        if (process_list[i] == NULL){
            return i;
        }
    }
    return -1; //si no hay mas lugar (i>MAX_PID = 50) => devuelvo -1 
}


//para cambiar la prioridad del proceso, dado su PID, y la nueva prioridad
//para el comando nice
int change_process_priority(int pid, int new_priority) {
    
    if ( pid >= MAX_PID || pid < 0 || process_list[pid] == NULL ) {
        return -1;
    }
    if ( new_priority < 0 || new_priority > 10 ) {
        return -2;
    }

    process_list[pid]->ppriority = new_priority;
    return 0;
}

int removeProcess_scheduler( int pid ) {

    if ( first_process == NULL ){
        // lista vacia
        return 1;
    }
    
    if (process_list[pid] == NULL ){
        // nunca deberia entrar aca por que ya lo checkee pero por las duuudas...
        return 1;
    }

    node_t *current = first_process;
    node_t *prev;
    while ( current !=NULL && current->pid != pid ) {
        prev = current;
        current = current->next;
    }
    if( current == NULL ) { 
        // llegue al final de la lista y no esta
        return 1;
    }
    else {
        
        if ( first_process->pid == pid ) {
            // era el primero de la lista
            if( first_process->next != NULL) {
                node_t *new_first = first_process->next;
                free(first_process);
                first_process = new_first->next;
                
            }
            else {
                // era el unico proceso en la lista
                free(first_process);
                first_process = NULL;
            }
        }
        else {
            prev->next = current->next;
        }
    }

    if ( process_list[pid]->status == RUNNING ) {
        current_process = NULL;
        
    }

    return 0;
}

int addProcess_scheduler(int pid) {

    if( process_list[pid] == NULL ) {
        return 1;
    }

    node_t *process =  (node_t *) alloc(sizeof(node_t));
    if (process == NULL) {
        return -2;
    }
    process->pid = pid;
    process->next = NULL;

    node_t *current =first_process;
    if (current == NULL ){

        first_process = process;
        current_process = first_process;

    } else {
         //no es el primer proceso ==>  lo agrego al final
        while(current->next != NULL ) {
            current=current->next;
        }
        current->next = process;
    }

    return 0;
}
 
// el estado puede ser READY o BLOCKED
int change_process_state(int pid, char state) {

    if ( pid >= MAX_PID || pid < 0 || process_list[pid] == NULL ) {
        // si entra aca dio pid=0 (proceso init), 
        // pid >= MAX_PID (imposible que un proceso tenga mayor a MAX_PID) 
        // o pid no asignado        
        return -1;
    }
    if ( state != READY && state != BLOCKED ) {
        // estado invalido
        return -2;
    }

    if ( state == BLOCKED ) {
        if( process_list[pid]->status != BLOCKED ) {
            // saco proceso de la lista del scheduler.
            // esta funcion maneja el caso de que el proceso se esté ejecutando.
            removeProcess_scheduler(pid);
            // si el proceso está siendo ejecutado, el status lo cambia el scheduler en la proxima interrupcion.
            // si no, actualizo el estado.
            if ( process_list[pid]->status != RUNNING) {
                process_list[pid]->status = BLOCKED;
            }
            else {
                prior_counter = -1;
            }

        }
        else {
            // ya estaba BLOCKED
            return 0;
        }
    }
    else {
        if ( process_list[pid]->status == BLOCKED ) {
            // se desbloquea el proceso, se agrega a la lista del scheduler.
            addProcess_scheduler(pid);
            process_list[pid]->status = READY;
        }
        else {
            // ya estaba READY
            return 0;
        }
    }
    return 0;
}

//Recorro los procesos y devuelvo el que esta corriendo
int get_pid() {
    for (int i = 0; i < MAX_PID; i++){
        if(process_list[i]->status == RUNNING ) 
            return i;
    }
    return -1;
}

// devuelvo el nodo del processo 
process_t* get_Pnode(int pid){
    if (pid >= MAX_PID || pid < 0){
        return NULL;
    }
    return process_list[pid];
}

// devuelve pid 
int get_Process_Pid(process_t* process){
    for (int i = 0; i < MAX_PID ; i++){
        if(process_list[i]== process){return i;}
    }
    return -1;
    
}

// Kill process
// para el comando kill
int kill(int pid) {

    if ( pid <= 0 || pid >= MAX_PID || process_list[pid] == NULL) {
        // si entra aca dio pid=0 (proceso init), 
        // pid >= MAX_PID (imposible que un proceso tenga mayor a MAX_PID) 
        // o pid no asignado 
        return 1;
    }

    removeProcess_scheduler(pid);

    free(process_list[pid]->base_pointer);
    free(process_list[pid]);
    process_list[pid] = NULL;

    return 0;
}

int is_blocked(int pid) {
    if ( process_list[pid]->status == BLOCKED ){
        return 1;
    }
    return 0;
}

void print_process(){
    print("\nPID           |PROCESS NAME      |STATUS        |PRIORITY      |\n");
    for(int i = 0 ; i < MAX_PID ; i++){
        if(process_list[i] != NULL  ){
            print("%d             ",i);
            print("%s               ",process_list[i]->name);
            if(process_list[i]->status == RUNNING){
                print("RUNNING           ");
            }
            else if(process_list[i]->status == READY){
                print("READY           ");
            }else if(process_list[i]->status == BLOCKED){
                print("BLOCKED           ");
            }else{
                print("UNDEFINED           ");
            }
            print("%d\n",process_list[i]->ppriority);
        }
    }
	
}
